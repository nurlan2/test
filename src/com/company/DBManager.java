package com.company;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;

public class DBManager {
    private Connection connection;
    public void getConnection() {
        try {
            connection= DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/onlineshopdb?useUnicode=true&serverTimezone=UTC", "postgres", "megatiger1998");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void addUser(User user){
        if(getUserByLogin(user.getUserLogin())!=null) {
            System.out.println("Account already exists!!");
            return;
        }
        try {
            PreparedStatement st=connection.prepareStatement(
                    "INSERT INTO USER_ACCOUNT(US_LOGIN,US_PASSWORD,US_ROLE,US_MONEY) VALUES(?,?,?,?)");
            st.setString(1,user.getUserLogin());
            st.setString(2,user.getUserPassword());
            st.setString(3,user.getUserRole());
            st.setInt(4,user.getAmountOfMoney());
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User getUserByLogin(String login) {
        User user=null;
        try {
            PreparedStatement st=connection.prepareStatement("SELECT*FROM USER_ACCOUNT WHERE US_LOGIN='"+login+"';");
            ResultSet rs=st.executeQuery();
            if(rs.next()) {
                user=new User
                        (rs.getLong("US_ID"),login,rs.getString("US_PASSWORD"),rs.getString("US_ROLE"),rs.getInt("US_MONEY"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }
    public void addProduct(Product product) {
        try {
            PreparedStatement st=connection.prepareStatement("INSERT INTO TB_PRODUCT(PR_NAME,PR_PRICE,PR_QUANTITY) VALUES(?,?,?)");
            st.setString(1,product.getNameOfProduct());
            st.setInt(2,product.getPrice());
            st.setInt(3,product.getQuantityOfProduct());
            st.executeUpdate();
            st.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public ArrayList<Product> listProducts() {
        ArrayList<Product> products=new ArrayList<>();
        try {
            PreparedStatement st=connection.prepareStatement("SELECT*FROM TB_PRODUCT");
            ResultSet rs=st.executeQuery();
            while (rs.next()) {
                Long id=rs.getLong("pr_id");
                String name=rs.getString("pr_name");
                int price=rs.getInt("pr_price");
                int quantity=rs.getInt("pr_quantity");
                products.add(new Product(id,name,price,quantity));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public void updateUser(User user){
        try {
            PreparedStatement st=connection.prepareStatement("UPDATE user_account set us_login=?,us_password=?,us_role=?,us_money=? where us_id=?");
            st.setString(1,user.getUserLogin());
            st.setString(2,user.getUserPassword());
            st.setString(3,user.getUserRole());
            st.setInt(4,user.getAmountOfMoney());
            st.setLong(5,user.getUserId());
            st.executeUpdate();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void updateProduct(Product product){
        try {
            PreparedStatement st=connection.prepareStatement("UPDATE tb_product set pr_name=?,pr_price=?,pr_quantity=? where pr_id=?");
            st.setString(1,product.getNameOfProduct());
            st.setInt(2,product.getPrice());
            st.setInt(3,product.getQuantityOfProduct());
            st.setLong(4,product.getProductId());
            st.executeUpdate();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void deleteProduct(Long id){
        try {
            PreparedStatement st=connection.prepareStatement("DELETE FROM tb_product where pr_id=?");
            st.setLong(1,id);
            st.executeUpdate();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Product selectProductbyID(Long id) {
        Product designatedProduct = null;
        try {
            PreparedStatement st=connection.prepareStatement("SELECT * FROM tb_product where pr_id=?");
            st.setLong(1,id);
            ResultSet rs = st.executeQuery();
            st.close();
            if (rs.next() == true) {
                designatedProduct = new Product(rs.getLong("pr_id"),
                        rs.getString("pr_name"),
                        rs.getInt("pr_price"),
                        rs.getInt("pr_quantity"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return designatedProduct;
    }

}




//    private Connection connection;
//    public void connect() {
//        try {
//            connection= DriverManager.getConnection(
//                    "jdbc:postgresql://localhost:5432/socialnetaccount?useUnicode=true&serverTimezone=UTC", "postgres", "megatiger1998");
//            )
//        }
//    }

