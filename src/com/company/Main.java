package com.company;

import org.mindrot.jbcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        DBManager db = new DBManager();
        db.getConnection();
        while (true) {
            System.out.println("[1]-create account");
            System.out.println("[2]-sign in to account");
            int choice = sc.nextInt();
            if (choice == 1) {
                System.out.println("Create login: ");
                String login = sc.next();
                System.out.println("Create password: ");
                String password = sc.next();
                System.out.println("Insert role: ");
                String role = sc.next();
                System.out.println("Amount of money: ");
                int money = sc.nextInt();

                User user = new User(null, login, password, role, money);
                db.addUser(user);
            } else if (choice == 2) {
                System.out.println("Insert login: ");
                String expectedLogin = sc.next();
                System.out.println("Insert password: ");
                String expectedPassword = sc.next();
//                String rehashed= BCrypt.hashpw(expectedPassword, BCrypt.gensalt(12));
                User currentUser = null;
                currentUser = db.getUserByLogin(expectedLogin);
                if (currentUser == null) {
                    System.out.println("ERROR, try again");
                    continue;
                }
                if (BCrypt.checkpw(expectedPassword, currentUser.getUserPassword())) {
                    System.out.println("ERROR");
                    continue;
                }

                if (currentUser.getUserRole().equals("admin")) {
                    while (true) {
                        System.out.println("1 - add product");
                        System.out.println("2 - list products");
                        System.out.println("3 - update product");
                        System.out.println("4 - delete product");
                        System.out.println("0 - exit");
                        int choiceAdmin = sc.nextInt();
                        if (choiceAdmin == 1) {
                            System.out.println("Name of product: ");
                            String name = sc.next();
                            System.out.println("Input price: ");
                            int price = sc.nextInt();
                            System.out.println("Quantity of product: ");
                            int quantity = sc.nextInt();
                            Product product = new Product(null, name, price, quantity);
                            db.addProduct(product);
                        } else if (choiceAdmin == 2) {
                            ArrayList<Product> products = db.listProducts();
                            for (Product p : products) {
                                System.out.println(p);
                            }
                        } else if (choiceAdmin == 3) {
                            System.out.println("Insert id :");
                            Long id = sc.nextLong();
                            System.out.println("Name of the new product: ");
                            String newName = sc.next();
                            System.out.println("insert price: ");
                            int price = sc.nextInt();
                            System.out.println("Quantity: ");
                            int quantity = sc.nextInt();
                            Product product = new Product(id, newName, price, quantity);
                            db.updateProduct(product);
                        } else if (choiceAdmin == 4) {
                            System.out.println("Insert id: ");
                            Long id = sc.nextLong();
                            db.deleteProduct(id);
                        } else if (choiceAdmin == 0 || choiceAdmin == 48) {
                            break;
                        }
                    }
                } else if (currentUser.getUserRole().equals("customer")) {
                    while (true) {
                        System.out.println("1 - list products");
                        System.out.println("2 - buy product");
                        System.out.println("0 - exit");
                        int choiceCustomer = sc.nextInt();
                        if (choiceCustomer == 1) {
                            ArrayList<Product> products = db.listProducts();
                            for (Product p : products) {
                                System.out.println(p);
                            }
                        } else if (choiceCustomer == 2) {
                            System.out.println("Insert id: ");
                            Product currentProduct = null;
                            Long id = sc.nextLong();
                            currentProduct = db.selectProductbyID(id);
                            if (currentProduct == null) {
                                System.out.println("There is no such account");
                                continue;
                            }
                            System.out.println("ID: " + currentProduct.getProductId() +
                                    "\tName: " + currentProduct.getNameOfProduct() +
                                    "\tPrice: " + currentProduct.getPrice() +
                                    "\tAmount: " + currentProduct.getQuantityOfProduct());
                            System.out.println("input amount of products: ");
                            int amount = sc.nextInt();
                            if (amount <= currentProduct.getQuantityOfProduct() && ((amount * currentProduct.getPrice()) <= currentUser.getAmountOfMoney())) {
                                currentProduct.setQuantityOfProduct(currentProduct.getQuantityOfProduct() - amount);
                                currentUser.setAmountOfMoney(currentUser.getAmountOfMoney() - amount * currentProduct.getPrice());
                                db.updateProduct(currentProduct);
                                db.updateUser(currentUser);
                                System.out.println("Operation succeeded: \nThe rest of the money: " + currentUser.getAmountOfMoney() +
                                        "\nThe rest of the product: " + currentProduct.getQuantityOfProduct());

                            } else {
                                System.out.println("Problem with money or amount of product");
                            }
                        } else if(choiceCustomer==0 && choiceCustomer==48) {
                            break;
                        }
                    }
                    // write your code here
                }
            }
        }
    }
}
