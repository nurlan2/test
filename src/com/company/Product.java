package com.company;

public class Product {
    private Long productId;
    private String nameOfProduct;
    private int price;
    private int quantityOfProduct;

    public Product(Long productId, String nameOfProduct, int price, int quantityOfProduct) {
        this.productId = productId;
        this.nameOfProduct = nameOfProduct;
        this.price = price;
        this.quantityOfProduct = quantityOfProduct;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantityOfProduct() {
        return quantityOfProduct;
    }

    public void setQuantityOfProduct(int quantityOfProduct) {
        this.quantityOfProduct = quantityOfProduct;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", nameOfProduct='" + nameOfProduct + '\'' +
                ", price=" + price +
                ", quantityOfProduct=" + quantityOfProduct +
                '}';
    }
}
