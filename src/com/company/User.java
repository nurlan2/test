package com.company;

import org.mindrot.jbcrypt.BCrypt;

public class User {
    private Long userId;
    private String userLogin;
    private String userPassword;
    private String userRole;
    private int amountOfMoney;

    public User(Long userId, String userLogin, String userPassword, String userRole, int amountOfMoney) {
        this.userId = userId;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userRole = userRole;
        this.amountOfMoney = amountOfMoney;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        String hashed = BCrypt.hashpw(userPassword, BCrypt.gensalt(12));
        return hashed;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userLogin='" + userLogin + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", amountOfMoney='" + amountOfMoney + '\'' +
                '}';
    }
}
